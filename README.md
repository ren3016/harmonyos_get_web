# HarmonyOS_GetWeb

#### 介绍
使用 @ohos.web.webview 嵌入远程网址

#### 开发平台

Win11 + DevEco Stuido

#### 开发步骤

1. 在 DevEco Studio 中 File->New_Create Project，选择 Application->Empty Ability， Next， Compile SDK：3.1.0(API 9)， Model: stage

2. 在默认的 Index.ets 文件第一行引入资源： import webview from '@ohos.web.webview';

```
import webview from '@ohos.web.webview';
```

3. 在 struct 中声明一个变量 myContorller：

```
  myContorller: webview.WebviewController = new webview.WebviewController()
```

4. 在 build 中 添加具有网页显示能力的Web组件：

```
  Web({
      src: "https://m.bilibili.com/",
      controller: this.myContorller
    })
```

5. 修改 entry\src\main\resources\base\element\string.json 文件中的 EntryAbility_label 的 value 为 GetWeb。 同时修改和 base 同级的 zh_CN、en_US 下的 string.json 文件中的 EntryAbility_label 的 value 为 GetWeb

6. 使用 USB 链接手机 并打开手机 USB 调试

7. 在 File 中选择 Project Structure -> Signing Configs 选择 Support HarmonyOS 和 Automatically generate signature （需要打开浏览器登录华为帐号并授权）

8. 链接手机成功后， 点击右上角工具栏的真机调试。

9. 手机上自动添加了名为 GetWeb 的应用

#### 主页代码

```
import webview from '@ohos.web.webview';

@Entry
@Component
struct Index {
  myContorller: webview.WebviewController = new webview.WebviewController()

  build() {
    Column() {
      // 直接使用网址
      Row() {
        Web({
          src: "https://m.bilibili.com/",
          controller: this.myContorller
        })
      }.height('50%')

      Divider().strokeWidth(8).color('#F1F3F5')
      // 网址带参数
      Row() {
        Web({
          src: "https://m.baidu.com/s?word=HarmonyOS DevEco",
          controller: this.myContorller
        })
      }.height('50%')
    }
    .width('100%')
  }
}
```
#### string.json 配置文件修改
1. entry\src\main\resources\base\element\string.json  （默认配置文件）
2. entry\src\main\resources\zh_CN\element\string.json  （中文配置文件）
3. entry\src\main\resources\en_US\element\string.json  （英文配置文件）
```
{
  "string": [
    {
      "name": "module_desc",
      "value": "module description"
    },
    {
      "name": "EntryAbility_desc",
      "value": "使用 @ohos.web.webview 嵌入远程网址"
    },
    {
      "name": "EntryAbility_label",
      "value": "GetWeb"
    }
  ]
}
```

#### 相关权限

1. 使用页面引入资源： import webview from '@ohos.web.webview';

2. 需要在文件中添加授权：entry\src\main\module.json5
```
"requestPermissions": [{
    "name": "ohos.permission.INTERNET"
}]
```

#### 克隆安装

git clone https://gitee.com/ren3016/harmonyos_get_web.git

#### 使用说明

打开应用，可以看到上面50%嵌入了B站首页，下面50%嵌入了百度带关键词搜索页。

#### 约束与限制
1. 本示例仅支持标准系统上运行，支持设备：华为手机或运行在DevEco Studio上的华为手机设备模拟器。
2. 本示例为Stage模型，支持API Version 9。
3. 本示例需要使用DevEco Studio 3.1 Release版本进行编译运行。

#### 页面效果
![输入图片说明](entry/src/main/resources/rawfile/Sample_image.jpg)

